# Программа для автоматического расчета всех величин в курсовой работе по дисциплине "Теория вероятностей и математическая статистика"

## 1. Сборка
- Для того, чтобы собрать приложение, нужен **Maven** и **Java 21**
- В терминале выполняем:
```shell
mvn clean package
```

## 2. Запуск
- После сборки, в папке `target` должен появиться файл `math.statistics.coursework-1.0.0.jar`
- Перед запуском, нужно в файл `src/main/resources/example_report_data.txt` вставить данные своего варианта
- Пример формата входных данных [здесь](https://gitlab.com/pyd4n/java-math-statistics-coursework/-/blob/main/src/main/resources/data/example_report_data.txt?ref_type=heads)

- И для запуска в терминале выполняем:
```shell
java -jar ./target/math.statistics.coursework-1.0.0.jar
```

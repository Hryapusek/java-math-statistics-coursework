package polytech.math.statistics.coursework.grouping.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Тест расчета эмпирической дисперсии.
 */
public class EmpiricalVarianceCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета эмпирической дисперсии на тестовых данных из отчета")
    public void exampleDataTest() {
        Assertions.assertEquals(BigDecimal.valueOf(33.588), empiricalVarianceCalculator
            .calculateEmpiricalVariance(getIntervals(TEST_VARIATION_SERIES)).setScale(3, RoundingMode.HALF_UP));
    }
}

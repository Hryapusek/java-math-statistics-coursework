package polytech.math.statistics.coursework.grouping;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;
import polytech.math.statistics.coursework.grouping.component.Interval;
import polytech.math.statistics.coursework.io.NumberedValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Тест группировки данных и расчета
 * всех параметров для всех интервалов.
 */
public class IntervalCalculatorTest extends BaseUnitTest {
    private final List<Interval> expectedIntervals = List.of(
        Interval.builder()
            .withNumber(1)
            .withStart(BigDecimal.valueOf(717.9))
            .withEnd(BigDecimal.valueOf(734.9))
            .withMiddle(BigDecimal.valueOf(726.4))
            .withFrequency(1)
            .withIncidence(BigDecimal.valueOf(0.005))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.005))
            .build(),
        Interval.builder()
            .withNumber(2)
            .withStart(BigDecimal.valueOf(734.9))
            .withEnd(BigDecimal.valueOf(751.9))
            .withMiddle(BigDecimal.valueOf(743.4))
            .withFrequency(5)
            .withIncidence(BigDecimal.valueOf(0.025))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.03))
            .build(),
        Interval.builder()
            .withNumber(3)
            .withStart(BigDecimal.valueOf(751.9))
            .withEnd(BigDecimal.valueOf(768.9))
            .withMiddle(BigDecimal.valueOf(760.4))
            .withFrequency(7)
            .withIncidence(BigDecimal.valueOf(0.035))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.065))
            .build(),
        Interval.builder()
            .withNumber(4)
            .withStart(BigDecimal.valueOf(768.9))
            .withEnd(BigDecimal.valueOf(785.9))
            .withMiddle(BigDecimal.valueOf(777.4))
            .withFrequency(12)
            .withIncidence(BigDecimal.valueOf(0.06))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.125))
            .build(),
        Interval.builder()
            .withNumber(5)
            .withStart(BigDecimal.valueOf(785.9))
            .withEnd(BigDecimal.valueOf(802.9))
            .withMiddle(BigDecimal.valueOf(794.4))
            .withFrequency(36)
            .withIncidence(BigDecimal.valueOf(0.18))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.305))
            .build(),
        Interval.builder()
            .withNumber(6)
            .withStart(BigDecimal.valueOf(802.9))
            .withEnd(BigDecimal.valueOf(819.9))
            .withMiddle(BigDecimal.valueOf(811.4))
            .withFrequency(34)
            .withIncidence(BigDecimal.valueOf(0.17))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.475))
            .build(),
        Interval.builder()
            .withNumber(7)
            .withStart(BigDecimal.valueOf(819.9))
            .withEnd(BigDecimal.valueOf(836.9))
            .withMiddle(BigDecimal.valueOf(828.4))
            .withFrequency(38)
            .withIncidence(BigDecimal.valueOf(0.19))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.665))
            .build(),
        Interval.builder()
            .withNumber(8)
            .withStart(BigDecimal.valueOf(836.9))
            .withEnd(BigDecimal.valueOf(853.9))
            .withMiddle(BigDecimal.valueOf(845.4))
            .withFrequency(32)
            .withIncidence(BigDecimal.valueOf(0.16))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.825))
            .build(),
        Interval.builder()
            .withNumber(9)
            .withStart(BigDecimal.valueOf(853.9))
            .withEnd(BigDecimal.valueOf(870.9))
            .withMiddle(BigDecimal.valueOf(862.4))
            .withFrequency(25)
            .withIncidence(BigDecimal.valueOf(0.125))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.95))
            .build(),
        Interval.builder()
            .withNumber(10)
            .withStart(BigDecimal.valueOf(870.9))
            .withEnd(BigDecimal.valueOf(887.9))
            .withMiddle(BigDecimal.valueOf(879.4))
            .withFrequency(5)
            .withIncidence(BigDecimal.valueOf(0.025))
            .withAccumulatedIncidence(BigDecimal.valueOf(0.975))
            .build(),
        Interval.builder()
            .withNumber(11)
            .withStart(BigDecimal.valueOf(887.9))
            .withEnd(BigDecimal.valueOf(904.9))
            .withMiddle(BigDecimal.valueOf(896.4))
            .withFrequency(5)
            .withIncidence(BigDecimal.valueOf(0.025))
            .withAccumulatedIncidence(BigDecimal.valueOf(1))
            .build()
    );

    @Test
    @DisplayName("Тест расчета интервалов на тестовых данных из отчета")
    public void exampleDataTest() {
        List<BigDecimal> numbers = fileDataReader
            .readNumbers(TEST_VARIATION_SERIES)
            .stream().map(NumberedValue::getValue).toList();

        List<Interval> intervals = getIntervals(TEST_VARIATION_SERIES);

        Assertions.assertEquals(numbers.size(), intervals.stream()
            .mapToInt(Interval::getFrequency).sum());
        Assertions.assertEquals(BigDecimal.valueOf(1),
            intervals.get(intervals.size() - 1).getAccumulatedIncidence()
                .setScale(0, RoundingMode.HALF_UP));

        AtomicInteger index = new AtomicInteger(0);
        intervals.forEach(interval -> Assertions.assertEquals(expectedIntervals
            .get(index.getAndIncrement()), interval));
    }
}

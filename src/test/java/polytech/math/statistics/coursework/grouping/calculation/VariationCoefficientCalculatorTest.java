package polytech.math.statistics.coursework.grouping.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Тест расчета коэффициента вариации.
 */
public class VariationCoefficientCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета коэффициента вариации на тестовых данных из отчета")
    public void exampleDataTest() {
        Assertions.assertEquals(BigDecimal.valueOf(0.040898), variationCoefficientCalculator
            .calculateVariationCoefficient(getIntervals(TEST_VARIATION_SERIES))
            .setScale(6, RoundingMode.HALF_UP));
    }
}

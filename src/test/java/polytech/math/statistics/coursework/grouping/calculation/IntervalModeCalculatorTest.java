package polytech.math.statistics.coursework.grouping.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Тест вычисления моды интервалов.
 */
public class IntervalModeCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета методы интервалов на тестовых данных из отчета")
    public void exampleDataTest() {
        Assertions.assertEquals(BigDecimal.valueOf(828.4),
            intervalModeCalculator.calculateIntervalMode(getIntervals(TEST_VARIATION_SERIES))
                .setScale(1, RoundingMode.HALF_UP));
    }
}

package polytech.math.statistics.coursework.homogeneity;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.util.List;

/**
 * Тест проверки выборки на однородность
 * по критерию знаков.
 */
public class SignCriteriaCalculatorTest extends BaseUnitTest {
    @Test
    public void exampleDataTest() {
        List<BigDecimal> numbers = getNumbers(TEST_DATA);
        Pair<Integer, Integer> parameters = signCriteriaCalculator
            .calculateSignCriteria(numbers, 20);
        Assertions.assertEquals(10, parameters.getLeft());
        Assertions.assertEquals(10, parameters.getRight());
    }
}

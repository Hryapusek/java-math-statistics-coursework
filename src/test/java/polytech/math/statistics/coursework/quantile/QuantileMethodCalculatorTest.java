package polytech.math.statistics.coursework.quantile;

import org.apache.commons.lang3.tuple.Pair;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;
import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Тест расчета параметров методом квантилей.
 */
public class QuantileMethodCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета параметров методом квантилей на тестовых данных из отчета")
    public void exampleDataTest() {
        List<Interval> intervals = getIntervals(TEST_VARIATION_SERIES);
        Pair<BigDecimal, BigDecimal> quantileParameters = quantileMethodCalculator
            .calculateQuantileMethodParameters(intervals, 2, 6);
        Assertions.assertEquals(BigDecimal.valueOf(813.745), quantileParameters.getLeft()
            .setScale(3, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(37.402), quantileParameters.getRight()
            .setScale(3, RoundingMode.HALF_UP));
    }
}

package polytech.math.statistics.coursework.kolmogorovsmirnov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;
import polytech.math.statistics.coursework.kolmogorovsmirnov.component.KolmSmirnovTableRow;
import polytech.math.statistics.coursework.grouping.component.Interval;
import polytech.math.statistics.coursework.io.NumberedValue;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class KolmSmirnovTest extends BaseUnitTest {
    private final List<KolmSmirnovTableRow> expectedRows = List.of(
            // 1
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(1)
                            .withStart(BigDecimal.valueOf(717.9))
                            .withEnd(BigDecimal.valueOf(734.9))
                            .withMiddle(BigDecimal.valueOf(726.4))
                            .withFrequency(1)
                            .withIncidence(BigDecimal.valueOf(0.005))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.005))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.004787))
                    .withDifference(BigDecimal.valueOf(0.005).subtract(BigDecimal.valueOf(0.004787)).abs())
                    .build(),

            // 2
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(2)
                            .withStart(BigDecimal.valueOf(734.9))
                            .withEnd(BigDecimal.valueOf(751.9))
                            .withMiddle(BigDecimal.valueOf(743.4))
                            .withFrequency(5)
                            .withIncidence(BigDecimal.valueOf(0.025))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.03))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.018765))
                    .withDifference(BigDecimal.valueOf(0.03).subtract(BigDecimal.valueOf(0.018765)).abs())
                    .build(),

            // 3
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(3)
                            .withStart(BigDecimal.valueOf(751.9))
                            .withEnd(BigDecimal.valueOf(768.9))
                            .withMiddle(BigDecimal.valueOf(760.4))
                            .withFrequency(7)
                            .withIncidence(BigDecimal.valueOf(0.035))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.065))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.058315))
                    .withDifference(BigDecimal.valueOf(0.065).subtract(BigDecimal.valueOf(0.058315)).abs())
                    .build(),

            // 4
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(4)
                            .withStart(BigDecimal.valueOf(768.9))
                            .withEnd(BigDecimal.valueOf(785.9))
                            .withMiddle(BigDecimal.valueOf(777.4))
                            .withFrequency(12)
                            .withIncidence(BigDecimal.valueOf(0.06))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.125))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.144983))
                    .withDifference(BigDecimal.valueOf(0.125).subtract(BigDecimal.valueOf(0.144983)).abs())
                    .build(),

            // 5
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(5)
                            .withStart(BigDecimal.valueOf(785.9))
                            .withEnd(BigDecimal.valueOf(802.9))
                            .withMiddle(BigDecimal.valueOf(794.4))
                            .withFrequency(36)
                            .withIncidence(BigDecimal.valueOf(0.18))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.305))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.292080))
                    .withDifference(BigDecimal.valueOf(0.305).subtract(BigDecimal.valueOf(0.292080)).abs())
                    .build(),

            // 6
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(6)
                            .withStart(BigDecimal.valueOf(802.9))
                            .withEnd(BigDecimal.valueOf(819.9))
                            .withMiddle(BigDecimal.valueOf(811.4))
                            .withFrequency(34)
                            .withIncidence(BigDecimal.valueOf(0.17))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.475))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.485467))
                    .withDifference(BigDecimal.valueOf(0.475).subtract(BigDecimal.valueOf(0.485467)).abs())
                    .build(),

            // 7
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(7)
                            .withStart(BigDecimal.valueOf(819.9))
                            .withEnd(BigDecimal.valueOf(836.9))
                            .withMiddle(BigDecimal.valueOf(828.4))
                            .withFrequency(38)
                            .withIncidence(BigDecimal.valueOf(0.19))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.665))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.682408))
                    .withDifference(BigDecimal.valueOf(0.665).subtract(BigDecimal.valueOf(0.682408)).abs())
                    .build(),

            // 8
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(8)
                            .withStart(BigDecimal.valueOf(836.9))
                            .withEnd(BigDecimal.valueOf(853.9))
                            .withMiddle(BigDecimal.valueOf(845.4))
                            .withFrequency(32)
                            .withIncidence(BigDecimal.valueOf(0.16))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.825))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.837767))
                    .withDifference(BigDecimal.valueOf(0.825).subtract(BigDecimal.valueOf(0.837767)).abs())
                    .build(),

            // 9
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(9)
                            .withStart(BigDecimal.valueOf(853.9))
                            .withEnd(BigDecimal.valueOf(870.9))
                            .withMiddle(BigDecimal.valueOf(862.4))
                            .withFrequency(25)
                            .withIncidence(BigDecimal.valueOf(0.125))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.95))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.932700))
                    .withDifference(BigDecimal.valueOf(0.95).subtract(BigDecimal.valueOf(0.932700)).abs())
                    .build(),

            // 10
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(10)
                            .withStart(BigDecimal.valueOf(870.9))
                            .withEnd(BigDecimal.valueOf(887.9))
                            .withMiddle(BigDecimal.valueOf(879.4))
                            .withFrequency(5)
                            .withIncidence(BigDecimal.valueOf(0.025))
                            .withAccumulatedIncidence(BigDecimal.valueOf(0.975))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.977630))
                    .withDifference(BigDecimal.valueOf(0.975).subtract(BigDecimal.valueOf(0.977630)).abs())
                    .build(),

            // 11
            KolmSmirnovTableRow.builder()
                    .withInterval(Interval.builder()
                            .withNumber(11)
                            .withStart(BigDecimal.valueOf(887.9))
                            .withEnd(BigDecimal.valueOf(904.9))
                            .withMiddle(BigDecimal.valueOf(896.4))
                            .withFrequency(5)
                            .withIncidence(BigDecimal.valueOf(0.025))
                            .withAccumulatedIncidence(BigDecimal.valueOf(1))
                            .build())
                    .withNormalDistrCumulativeIncidence(BigDecimal.valueOf(0.994098))
                    .withDifference(BigDecimal.valueOf(1).subtract(BigDecimal.valueOf(0.994098)).abs())
                    .build());

    @Test
    @DisplayName("Тест расчета хихихи квадрата на примере таблицы из отчета")
    public void exampleDataTest() {
        List<BigDecimal> allNumbers = fileDataReader
                .readNumbers(TEST_DATA)
                .stream().map(NumberedValue::getValue).toList();

        BigDecimal allExpectance = mathExpectationCalculator
                .calculateMathExpectation(allNumbers);

        BigDecimal allDeviation = dispersionCalculator
                .calculateBiasedEstimation(allNumbers);

        List<Interval> intervals = getIntervals(TEST_VARIATION_SERIES);

        var table = kolmSmirnovCalculator.calculateKolmSmirnovTable(intervals, allExpectance, allDeviation);
        var rows = table.getRows();
        Assertions.assertEquals(expectedRows.size(), rows.size());
        for (int i = 0; i < expectedRows.size(); ++i) {
            var currentRow = rows.get(i);
            var expectedRow = expectedRows.get(i);

            Assertions.assertEquals(expectedRow.getNormalDistrCumulativeIncidence().setScale(3, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getNormalDistrCumulativeIncidence().setScale(3, RoundingMode.HALF_UP).doubleValue());

            Assertions.assertEquals(expectedRow.getInterval().getAccumulatedIncidence().setScale(3, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getInterval().getAccumulatedIncidence().setScale(3, RoundingMode.HALF_UP).doubleValue());

            Assertions.assertEquals(expectedRow.getDifference().setScale(3, RoundingMode.HALF_UP).doubleValue(),
                    currentRow.getDifference().setScale(3, RoundingMode.HALF_UP).doubleValue());
        }
        Assertions.assertEquals(0.02, table.getMaxDifference().setScale(2, RoundingMode.HALF_UP).doubleValue());
    }
}

package polytech.math.statistics.coursework.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Тесты для расчета математического ожидания.
 */
public class MathExpectationCalculatorTest extends BaseUnitTest {

    @Test
    @DisplayName("Тест расчета математического ожидания с одним элементом")
    public void testOneElementList() {
        List<BigDecimal> numbers = List.of(BigDecimal.valueOf(12.345));
        Assertions.assertEquals(BigDecimal.valueOf(12.35),
            mathExpectationCalculator.calculateMathExpectation(numbers)
                .setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    @DisplayName("Тест расчета математического ожидания на тестовых данных из отчета")
    public void exampleDataTest() {
        List<BigDecimal> numbers = getNumbers(TEST_DATA);
        Assertions.assertEquals(BigDecimal.valueOf(821.11),
            mathExpectationCalculator.calculateMathExpectation(numbers)
                .setScale(2, RoundingMode.HALF_UP));
    }
}

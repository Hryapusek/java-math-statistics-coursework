package polytech.math.statistics.coursework.calculation;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Тест расчета оценки медианы.
 */
public class MedianEstimationCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест расчета оценки медианы на тестовых данных из отчета")
    public void exampleDataTest() {
        List<BigDecimal> numbers = getNumbers(TEST_VARIATION_SERIES);

        BigDecimal medianEstimation = medianEstimationCalculator
            .calculateMedianEstimation(numbers);

        Assertions.assertEquals(BigDecimal.valueOf(821.65),
            medianEstimation.setScale(2, RoundingMode.HALF_UP));
    }
}

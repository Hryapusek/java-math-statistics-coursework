package polytech.math.statistics.coursework;

import polytech.math.statistics.coursework.calculation.DispersionCalculator;
import polytech.math.statistics.coursework.calculation.MathExpectationCalculator;
import polytech.math.statistics.coursework.calculation.MedianEstimationCalculator;
import polytech.math.statistics.coursework.calculation.VariationRangeCalculator;
import polytech.math.statistics.coursework.chisquare.ChiSquareCalculator;
import polytech.math.statistics.coursework.function.InverseNormalDistributionFunctionCalculator;
import polytech.math.statistics.coursework.grouping.IntervalCalculator;
import polytech.math.statistics.coursework.grouping.calculation.EmpiricalVarianceCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalAsymmetryCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalAverageCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalExcessCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalModeCalculator;
import polytech.math.statistics.coursework.grouping.calculation.SheppardCorrectionEmpiricalVarianceCalculator;
import polytech.math.statistics.coursework.grouping.calculation.VariationCoefficientCalculator;
import polytech.math.statistics.coursework.grouping.component.Interval;
import polytech.math.statistics.coursework.homogeneity.SignCriteriaCalculator;
import polytech.math.statistics.coursework.io.FileDataReader;
import polytech.math.statistics.coursework.io.NumberedValue;
import polytech.math.statistics.coursework.kolmogorovsmirnov.KolmSmirnovCalculator;
import polytech.math.statistics.coursework.quantile.QuantileMethodCalculator;
import polytech.math.statistics.coursework.trusting.DispersionTrustedIntervalCalculator;
import polytech.math.statistics.coursework.trusting.MathExpectationTrustIntervalCalculator;
import polytech.math.statistics.coursework.wilcoxon.WilcoxonCriteriaCalculator;

import java.math.BigDecimal;
import java.util.List;

/**
 * Базовый класс со всеми необходимыми полями
 * для unit-тестов.
 */
public class BaseUnitTest {
    /**
     * Название файла с нумерованными денормализованными
     * числами из примера отчета.
     */
    protected static final String TEST_DATA = "test-data/example_report_data.txt";

    /**
     * Название файла с вариационным рядом
     * из примера отчета.
     */
    protected static final String TEST_VARIATION_SERIES = "test-data/example_report_variation_series.txt";

    /**
     * Название второго файла с вариационным рядом
     * из примера отчета.
     */
    protected static final String SECOND_TEST_VARIATION_SERIES = "test-data/example_report_variation_series_2.txt";

    /**
     * Класс для чтения строк с числами из файла.
     */
    protected final FileDataReader fileDataReader =
        new FileDataReader();

    /**
     * Класс для расчета математического ожидания.
     */
    protected final MathExpectationCalculator mathExpectationCalculator =
        new MathExpectationCalculator();

    /**
     * Класс для вычисления смещенной и несмещенной оценки дисперсии
     * по заданной выборке чисел.
     */
    protected final DispersionCalculator dispersionCalculator =
        new DispersionCalculator();

    /**
     * Класс для расчета оценки медианы по заданной выборке чисел.
     */
    protected final MedianEstimationCalculator medianEstimationCalculator =
        new MedianEstimationCalculator();

    /**
     * Класс для расчета размаха варьирования.
     */
    protected final VariationRangeCalculator variationRangeCalculator =
        new VariationRangeCalculator();

    /**
     * Класс для группировки данных и расчета
     * всех параметров для всех интервалов.
     */
    protected final IntervalCalculator intervalCalculator =
        new IntervalCalculator((numbers, intervalWidth) -> BigDecimal.valueOf(717.9));

    /**
     * Класс для расчета среднего арифметического
     * по заданным интервалам.
     */
    protected final IntervalAverageCalculator intervalAverageCalculator =
        new IntervalAverageCalculator();

    /**
     * Класс для вычисления моды интервалов.
     */
    protected final IntervalModeCalculator intervalModeCalculator =
        new IntervalModeCalculator();

    /**
     * Класс для расчета эмпирической дисперсии.
     */
    protected final EmpiricalVarianceCalculator empiricalVarianceCalculator =
        new EmpiricalVarianceCalculator();

    /**
     * Класс для расчета коэффициента вариации.
     */
    protected final VariationCoefficientCalculator variationCoefficientCalculator =
        new VariationCoefficientCalculator();

    /**
     * Расчет эмпирическую дисперсию с учетом смещения с помощью
     * поправки Шеппарда.
     */
    protected final SheppardCorrectionEmpiricalVarianceCalculator sheppardCorrectionEmpiricalVarianceCalculator =
        new SheppardCorrectionEmpiricalVarianceCalculator();

    /**
     * Класс для расчета асимметрии.
     */
    protected final IntervalAsymmetryCalculator intervalAsymmetryCalculator =
        new IntervalAsymmetryCalculator();

    /**
     * Класс для расчета эксцесса.
     */
    protected final IntervalExcessCalculator intervalExcessCalculator =
        new IntervalExcessCalculator();

    /**
     * Класс для нахождения параметров m и б (сигма)
     * методом квантилей.
     */
    protected final QuantileMethodCalculator quantileMethodCalculator =
        new QuantileMethodCalculator();

    /**
     * Класс для расчета доверительного интервала
     * для математического ожидания.
     */
    protected final MathExpectationTrustIntervalCalculator mathExpectationTrustIntervalCalculator =
        new MathExpectationTrustIntervalCalculator();

    /**
     * Класс для вычисления доверительного интервала для
     * дисперсии и среднего квадратического отклонения.
     */
    protected final DispersionTrustedIntervalCalculator dispersionTrustedIntervalCalculator =
        new DispersionTrustedIntervalCalculator();

    /**
     * Обратная функция нормального распределения (даем значение - получаем аргумент).
     * По сути функция НОРМ.СТ.ОБР из Экселя.
     */
    protected final InverseNormalDistributionFunctionCalculator inverseNormalDistributionFunctionCalculator =
        new InverseNormalDistributionFunctionCalculator();

    /**
     * Класс для проверки выборки на однородность
     * по критерию знаков.
     */
    protected final SignCriteriaCalculator signCriteriaCalculator =
        new SignCriteriaCalculator();

    /**
     * Класс для расчета числа инверсий
     * по критерию Вилкоксона.
     */
    protected final WilcoxonCriteriaCalculator wilcoxonCriteriaCalculator =
        new WilcoxonCriteriaCalculator();

    /**
     * Класс для расчета Хи квадрата.
     */
    protected final ChiSquareCalculator chiSquareCalculator =
        new ChiSquareCalculator();

    /**
     * 
     * @param fileName
     * @return
     */
    protected final KolmSmirnovCalculator kolmSmirnovCalculator =
        new KolmSmirnovCalculator();

    /**
     * Получить числовые данные из файла.
     *
     * @param fileName - имя файла
     * @return список чисел
     */
    protected List<BigDecimal> getNumbers(String fileName) {
        return fileDataReader.readNumbers(fileName)
            .stream().map(NumberedValue::getValue).toList();
    }

    /**
     * Получить интервалы для тестовых данных.
     *
     * @param fileName - имя файла с числами
     * @return интервалы
     */
    protected List<Interval> getIntervals(String fileName) {
        List<BigDecimal> numbers = getNumbers(fileName);

        return intervalCalculator
            .calculateIntervals(numbers, 11);
    }
}

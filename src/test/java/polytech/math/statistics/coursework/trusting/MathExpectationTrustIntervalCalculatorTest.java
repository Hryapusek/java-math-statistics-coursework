package polytech.math.statistics.coursework.trusting;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public class MathExpectationTrustIntervalCalculatorTest extends BaseUnitTest {
    @Test
    public void exampleDataTest() {
        List<BigDecimal> numbers = getNumbers(SECOND_TEST_VARIATION_SERIES);
        Assertions.assertEquals(BigDecimal.valueOf(820.905), mathExpectationTrustIntervalCalculator
            .calculateAverage(numbers).setScale(3, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(39.8), mathExpectationTrustIntervalCalculator
            .calculateDispersion(numbers).setScale(1, RoundingMode.HALF_UP));
        // q = 5%, t_5,19 = 2.093
        Assertions.assertEquals(BigDecimal.valueOf(801.7943), mathExpectationTrustIntervalCalculator
            .calculateLeftBorder(numbers, BigDecimal.valueOf(2.093)).setScale(4, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(840.0157), mathExpectationTrustIntervalCalculator
            .calculateRightBorder(numbers, BigDecimal.valueOf(2.093)).setScale(4, RoundingMode.HALF_UP));

        // q = 1%, t_5,19 = 2.861
        Assertions.assertEquals(BigDecimal.valueOf(794.7819), mathExpectationTrustIntervalCalculator
            .calculateLeftBorder(numbers, BigDecimal.valueOf(2.861)).setScale(4, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(847.0281), mathExpectationTrustIntervalCalculator
            .calculateRightBorder(numbers, BigDecimal.valueOf(2.861)).setScale(4, RoundingMode.HALF_UP));

        // q = 10%, t_5,19 = 1.729
        Assertions.assertEquals(BigDecimal.valueOf(805.1179), mathExpectationTrustIntervalCalculator
            .calculateLeftBorder(numbers, BigDecimal.valueOf(1.729)).setScale(4, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(836.6921), mathExpectationTrustIntervalCalculator
            .calculateRightBorder(numbers, BigDecimal.valueOf(1.729)).setScale(4, RoundingMode.HALF_UP));
    }
}

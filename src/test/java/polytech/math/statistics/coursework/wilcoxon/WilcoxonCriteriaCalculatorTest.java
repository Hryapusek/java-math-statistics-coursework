package polytech.math.statistics.coursework.wilcoxon;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Тест расчета числа инверсий
 * по критерию Вилкоксона.
 */
public class WilcoxonCriteriaCalculatorTest extends BaseUnitTest {
    @Test
    public void inversionNumberTest() {
        List<BigDecimal> numbers = getNumbers(TEST_DATA);
        Assertions.assertEquals(191, wilcoxonCriteriaCalculator
            .calculateInversionsNumber(numbers, 20));
    }

    @Test
    public void tValueCalculationTest() {
        // q = 5%
        Assertions.assertEquals(BigDecimal.valueOf(1.96), wilcoxonCriteriaCalculator
            .calculateTValue(5)
            .setScale(2, RoundingMode.HALF_UP));
        // q = 1%
        Assertions.assertEquals(BigDecimal.valueOf(2.58), wilcoxonCriteriaCalculator
            .calculateTValue(1)
            .setScale(2, RoundingMode.HALF_UP));
        // q = 10%
        Assertions.assertEquals(BigDecimal.valueOf(1.64), wilcoxonCriteriaCalculator
            .calculateTValue(10)
            .setScale(2, RoundingMode.HALF_UP));
    }

    @Test
    public void bordersTest() {
        // q = 5%
        Assertions.assertEquals(BigDecimal.valueOf(127.54), wilcoxonCriteriaCalculator
            .calculateBorders(20, 5).getLeft()
            .setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(272.46), wilcoxonCriteriaCalculator
            .calculateBorders(20, 5).getRight()
            .setScale(2, RoundingMode.HALF_UP));
        // q = 1%
        Assertions.assertEquals(BigDecimal.valueOf(104.78), wilcoxonCriteriaCalculator
            .calculateBorders(20, 1).getLeft()
            .setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(295.22), wilcoxonCriteriaCalculator
            .calculateBorders(20, 1).getRight()
            .setScale(2, RoundingMode.HALF_UP));
        // q = 10%
        Assertions.assertEquals(BigDecimal.valueOf(139.19), wilcoxonCriteriaCalculator
            .calculateBorders(20, 10).getLeft()
            .setScale(2, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(260.81), wilcoxonCriteriaCalculator
            .calculateBorders(20, 10).getRight()
            .setScale(2, RoundingMode.HALF_UP));
    }
}

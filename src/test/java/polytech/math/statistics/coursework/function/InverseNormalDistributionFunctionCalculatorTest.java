package polytech.math.statistics.coursework.function;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import polytech.math.statistics.coursework.BaseUnitTest;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Тест вычисления обратной функции нормального распределения.
 */
public class InverseNormalDistributionFunctionCalculatorTest extends BaseUnitTest {
    @Test
    @DisplayName("Тест сравнения значений с функцией НОРМ.СТ.ОБР из экселя")
    public void excelComparisonTest() {
        Assertions.assertEquals(BigDecimal.valueOf(-0.706303), inverseNormalDistributionFunctionCalculator
            .getParameterByValue(BigDecimal.valueOf(0.24))
                .setScale(6, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(0.38532), inverseNormalDistributionFunctionCalculator
            .getParameterByValue(BigDecimal.valueOf(0.65))
            .setScale(5, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(2.326348), inverseNormalDistributionFunctionCalculator
            .getParameterByValue(BigDecimal.valueOf(0.99))
            .setScale(6, RoundingMode.HALF_UP));
        Assertions.assertEquals(BigDecimal.valueOf(-2.575829), inverseNormalDistributionFunctionCalculator
            .getParameterByValue(BigDecimal.valueOf(0.005))
            .setScale(6, RoundingMode.HALF_UP));
    }
}

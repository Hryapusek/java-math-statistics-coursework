package polytech.math.statistics.coursework.trusting;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета доверительного интервала
 * для математического ожидания.
 */
public class MathExpectationTrustIntervalCalculator {
    /**
     * Расчет среднего значения из выборки.
     *
     * @param numbers - выборка чисел
     * @return среднее значение
     */
    public BigDecimal calculateAverage(List<BigDecimal> numbers) {
        return numbers.stream().reduce(BigDecimal.ZERO, BigDecimal::add)
            .divide(BigDecimal.valueOf(numbers.size()), 10, RoundingMode.HALF_UP);
    }

    /**
     * Расчет дисперсии (?) выборки.
     *
     * @param numbers - выборка чисел
     * @return дисперсия (?)
     */
    public BigDecimal calculateDispersion(List<BigDecimal> numbers) {
        BigDecimal average = calculateAverage(numbers);
        BigDecimal averageOfSquared = numbers.stream().reduce(BigDecimal.ZERO, (acc, number) -> acc.add(number.pow(2)
            .divide(BigDecimal.valueOf(numbers.size()), 10, RoundingMode.HALF_UP)));

        return averageOfSquared.subtract(average.pow(2)).sqrt(MathContext.DECIMAL128)
            .setScale(10, RoundingMode.HALF_UP);
    }

    /**
     * Расчет левой границы доверительного интервала
     * для математического ожидания
     *
     * @param numbers - выборка чисел
     * @param studentCoefficient - коэффициент Стьюдента при заданном уровне значимости q
     * @return левая граница доверительного интервала для математического ожидания
     */
    public BigDecimal calculateLeftBorder(List<BigDecimal> numbers, BigDecimal studentCoefficient) {
        BigDecimal average = calculateAverage(numbers);
        BigDecimal fraction = calculateDispersion(numbers).divide(BigDecimal.valueOf(numbers.size() - 1)
            .sqrt(MathContext.DECIMAL128), 10, RoundingMode.HALF_UP);

        return average.subtract(studentCoefficient.multiply(fraction));
    }

    /**
     * Расчет правой границы доверительного интервала
     * для математического ожидания
     *
     * @param numbers - выборка чисел
     * @param studentCoefficient - коэффициент Стьюдента при заданном уровне значимости q
     * @return правая граница доверительного интервала для математического ожидания
     */
    public BigDecimal calculateRightBorder(List<BigDecimal> numbers, BigDecimal studentCoefficient) {
        BigDecimal average = calculateAverage(numbers);
        BigDecimal fraction = calculateDispersion(numbers).divide(BigDecimal.valueOf(numbers.size() - 1)
            .sqrt(MathContext.DECIMAL128), 10, RoundingMode.HALF_UP);

        return average.add(studentCoefficient.multiply(fraction));
    }
}

package polytech.math.statistics.coursework.util;

import lombok.Data;
import polytech.math.statistics.coursework.io.FileDataReader;
import polytech.math.statistics.coursework.io.NumberedValue;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;

/**
 * Класс для упорядочивания строк в файлах с данными.
 */
@Data
public class SortedFileCreator {
    /**
     * Класс для чтения строк из файла.
     */
    private final FileDataReader fileDataReader =
        new FileDataReader();

    /**
     * Отсортировать файл.
     *
     * @param unsortedFileName - имя файла с неотсортированными строками
     * @param sortedFileName   - имя файла, куда будут записаны отсортированные строки
     */
    public void fillSortedFile(String unsortedFileName,
                               String sortedFileName) {
        try (var writer = new BufferedWriter(new FileWriter(sortedFileName))) {
            List<NumberedValue> rows = fileDataReader.readNumbers(unsortedFileName);
            rows.sort(Comparator.comparingInt(NumberedValue::getRowNumber));
            writeSortedRows(writer, rows);
        } catch (Exception e) {
            throw new IllegalArgumentException("Ошибка при чтении файла: "
                + unsortedFileName + " или " + sortedFileName);
        }
    }

    /**
     * Записать отсортированные строки в файл
     * @param writer - writer для записи в файл
     * @param rows - отсортированные строки
     * @throws IOException - при ошибке записи в файл
     */
    private void writeSortedRows(BufferedWriter writer,
                                 List<NumberedValue> rows) throws IOException {
        for (NumberedValue row : rows) {
            writer.write(String.format("%d %s", row.getRowNumber(),
                row.getValue()));
            writer.newLine();
        }
    }
}

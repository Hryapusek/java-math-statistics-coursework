package polytech.math.statistics.coursework.kolmogorovsmirnov;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;

import polytech.math.statistics.coursework.grouping.component.Interval;
import polytech.math.statistics.coursework.kolmogorovsmirnov.component.KolmSmirnovTable;
import polytech.math.statistics.coursework.kolmogorovsmirnov.component.KolmSmirnovTableRow;

/**
 * Класс для расчёта критерия Колмогорова-Смирнова
 */
public class KolmSmirnovCalculator {
    /**
     * Рассчитать критерий Колмогорова-Смирнова для заданных интервалов
     * с использованием нормального распределения.
     *
     * @param intervals      - интервалы
     * @param expectance     - математическое ожидание, полученное из всех
     *                       элементов таблицы. Из самого первого раздела
     * @param deviation      -смещенная дисперсия, полученная из всех
     *                       элементов таблицы
     *                       Из раздела доверительный интервал
     * @return таблицу, связанную с критерием Колмогорова-Смирнова
     */
    public KolmSmirnovTable calculateKolmSmirnovTable(List<Interval> intervals,
            BigDecimal expectance, BigDecimal deviation) {
        KolmSmirnovTable resultTable = new KolmSmirnovTable();
        var normalDistribution = new NormalDistribution();
        for (var interval : intervals) {
            var rowBuilder = KolmSmirnovTableRow.builder();
            rowBuilder.withInterval(interval);

            var currentDistribution = normalDistribution.cumulativeProbability(
                    interval.getEnd().subtract(expectance).divide(deviation, RoundingMode.HALF_UP).doubleValue());
            rowBuilder.withNormalDistrCumulativeIncidence(BigDecimal.valueOf(currentDistribution));

            var difference = interval.getAccumulatedIncidence().subtract(BigDecimal.valueOf(currentDistribution));
            rowBuilder.withDifference(difference.abs());

            resultTable.addRow(rowBuilder.build());
        }
        return resultTable;
    }
}

package polytech.math.statistics.coursework.kolmogorovsmirnov.component;

import java.math.BigDecimal;
import java.math.RoundingMode;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import polytech.math.statistics.coursework.grouping.component.Interval;

@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class KolmSmirnovTableRow {
    /**
     * Интервал связанный с этой строчкой
     */
    private final Interval interval;

    /**
     * Накопленная частотность нормального распределения
     */
    private final BigDecimal normalDistrCumulativeIncidence;

    /**
     * Разица между эмпирической и нормальной частотностью
     */
    private final BigDecimal difference;

    @Override
    public String toString() {
        var intervalBeginStr = String.valueOf(interval.getStart());
        var intervalEndStr = String.valueOf(interval.getEnd());
        var empiricalDistrCumulativeIncidenceStr = String.valueOf(interval.getAccumulatedIncidence()
                .setScale(5, RoundingMode.HALF_UP));
        var normalDistrCumulativeIncidenceStr = String
                .valueOf(normalDistrCumulativeIncidence.setScale(5, RoundingMode.HALF_UP));
        var differenceStr = String.valueOf(difference.setScale(5, RoundingMode.HALF_UP));
        return String.format("%7s%7s%15s%15s%15s",
                intervalBeginStr, intervalEndStr, empiricalDistrCumulativeIncidenceStr,
                normalDistrCumulativeIncidenceStr, differenceStr);
    }
}

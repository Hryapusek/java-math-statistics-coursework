package polytech.math.statistics.coursework.kolmogorovsmirnov.component;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;

import lombok.Data;

@Data
public class KolmSmirnovTable {
    private ArrayList<KolmSmirnovTableRow> rows = new ArrayList<>();

    /**
     * Добавляет строчку в конец таблицы
     */
    public void addRow(KolmSmirnovTableRow row) {
        rows.addLast(row);
    }

    /**
     * @return Максимальную разницу между эмперическим и нормальным распределением
     */
    public BigDecimal getMaxDifference() {
        return rows.stream().map(KolmSmirnovTableRow::getDifference)
                        .max(Comparator.naturalOrder()).get();
    }

    @Override
    public String toString() {
        StringBuilder resultStr = new StringBuilder();
        String[] headers = new String[] { "N", "Начало", "Конец", "Эмпер распр", "Норм распр", "Расход" };
        resultStr.append(String.format("%2s%7s%7s%30s%15s%n", "", "", "", "Накопленная частотность", ""));
        resultStr.append(String.format("%2s%7s%7s%15s%15s%15s%n", (Object[]) headers));
        for (int i = 0; i < rows.size(); ++i) {
            resultStr.append(String.format("%2s", String.valueOf(i+1)));
            resultStr.append(rows.get(i).toString());
            resultStr.append("\n");
        }
        return resultStr.toString();
    }
}

package polytech.math.statistics.coursework.quantile;

import org.apache.commons.lang3.tuple.Pair;
import polytech.math.statistics.coursework.function.InverseNormalDistributionFunctionCalculator;
import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для нахождения параметров m и б (сигма)
 * методом квантилей.
 */
public class QuantileMethodCalculator {
    /**
     * Обратная функция нормального распределения (даем значение - получаем аргумент).
     * По сути функция НОРМ.СТ.ОБР из Экселя.
     */
    private final InverseNormalDistributionFunctionCalculator inverseNormalDistributionFunctionCalculator =
        new InverseNormalDistributionFunctionCalculator();

    /**
     * Вычисление параметров m и б (сигма) методом квантилей.
     *
     * @param intervals            - интервалы
     * @param firstIntervalNumber  - номер первого интервала для расчета (1-indexed)
     * @param secondIntervalNumber - номер второго интервала для расчета (1-indexed)
     * @return пара параметров m и б (сигма)
     */
    public Pair<BigDecimal, BigDecimal> calculateQuantileMethodParameters(List<Interval> intervals,
                                                                          Integer firstIntervalNumber,
                                                                          Integer secondIntervalNumber) {
        BigDecimal x1 = intervals.get(firstIntervalNumber - 1).getMiddle();
        BigDecimal x2 = intervals.get(secondIntervalNumber - 1).getMiddle();

        BigDecimal p1 = intervals.get(firstIntervalNumber - 1).getAccumulatedIncidence();
        BigDecimal p2 = intervals.get(secondIntervalNumber -1).getAccumulatedIncidence();

        BigDecimal a = inverseNormalDistributionFunctionCalculator.getParameterByValue(p1);
        BigDecimal b = inverseNormalDistributionFunctionCalculator.getParameterByValue(p2);

        BigDecimal sigma = x1.subtract(x2).divide(a.subtract(b), 10, RoundingMode.HALF_UP);
        BigDecimal m = x1.subtract(a.multiply(sigma));

        return Pair.of(m, sigma);
    }
}

package polytech.math.statistics.coursework.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета размаха варьирования.
 */
public class VariationRangeCalculator {
    /**
     * Рассчитать размах варьирования.
     *
     * @param numbers - числа вариационного ряда
     * @return размах варьирования
     */
    public BigDecimal calculateVariationRange(List<BigDecimal> numbers) {
        return numbers.get(numbers.size() - 1).subtract(numbers.get(0))
            .setScale(10, RoundingMode.HALF_UP);
    }
}

package polytech.math.statistics.coursework.calculation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета оценки медианы по заданной выборке чисел.
 */
public class MedianEstimationCalculator {
    /**
     * Рассчитать оценку медианы по заданной выборке.
     * @param numbers - числа вариационного ряда
     * @return оценка медианы
     */
    public BigDecimal calculateMedianEstimation(List<BigDecimal> numbers) {
        int k = (numbers.size() / 2) - 1;
        return numbers.get(k).add(numbers.get(k + 1))
            .divide(BigDecimal.TWO, 10, RoundingMode.HALF_UP);
    }
}

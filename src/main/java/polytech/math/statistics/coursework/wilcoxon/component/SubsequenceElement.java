package polytech.math.statistics.coursework.wilcoxon.component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Элемент подвыборки.
 */
@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class SubsequenceElement implements Comparable<SubsequenceElement> {
    /**
     * Номер подвыборки (0, если элемент из начала, 1, если из конца)
     */
    private final Integer subsequenceNumber;

    /**
     * Значение.
     */
    private final BigDecimal value;

    @Override
    public int compareTo(SubsequenceElement another) {
        return value.setScale(1, RoundingMode.HALF_UP)
            .compareTo(another.value.setScale(1, RoundingMode.HALF_UP));
    }
}

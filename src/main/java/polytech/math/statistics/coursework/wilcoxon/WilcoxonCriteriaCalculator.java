package polytech.math.statistics.coursework.wilcoxon;

import org.apache.commons.lang3.tuple.Pair;
import polytech.math.statistics.coursework.function.InverseNormalDistributionFunctionCalculator;
import polytech.math.statistics.coursework.wilcoxon.component.SubsequenceElement;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Класс для расчета числа инверсий
 * по критерию Вилкоксона.
 */
public class WilcoxonCriteriaCalculator {
    /**
     * Обратная функция нормального распределения (даем значение - получаем аргумент).
     * По сути функция НОРМ.СТ.ОБР из Экселя.
     */
    private final InverseNormalDistributionFunctionCalculator inverseNormalDistributionFunctionCalculator =
        new InverseNormalDistributionFunctionCalculator();

    /**
     * Рассчитать количество инверсий.
     * @param numbers - выборка чисел
     * @param subsequenceSize - сколько чисел с начала и с конца взять
     * @return количество инверсий
     */
    public Integer calculateInversionsNumber(List<BigDecimal> numbers,
                                             Integer subsequenceSize) {
        List<BigDecimal> numbersCopy = new ArrayList<>(numbers);
        List<SubsequenceElement> firstSubsequence = numbersCopy.stream().limit(subsequenceSize)
            .map(number -> SubsequenceElement.builder()
                .withSubsequenceNumber(0)
                .withValue(number)
                .build()).toList();
        Collections.reverse(numbersCopy);
        List<SubsequenceElement> secondSubsequence = new ArrayList<>(numbersCopy.stream().limit(subsequenceSize)
            .map(number -> SubsequenceElement.builder()
                .withSubsequenceNumber(1)
                .withValue(number)
                .build()).toList());
        Collections.reverse(secondSubsequence);

        List<SubsequenceElement> allNumbers = new ArrayList<>(firstSubsequence);
        allNumbers.addAll(secondSubsequence);
        Collections.sort(allNumbers);

        int inversionsCount = 0;
        for (int current = 0; current < allNumbers.size(); current++) {
            for (int prev = 0; prev < current; prev++) {
                if (allNumbers.get(prev).getSubsequenceNumber() > allNumbers.get(current).getSubsequenceNumber()) {
                    inversionsCount++;
                }
            }
        }

        return inversionsCount;
    }

    /**
     * Рассчитать значение t.
     *
     * @param q - уровень значимости
     * @return значение t
     */
    public BigDecimal calculateTValue(Integer q) {
        BigDecimal a = BigDecimal.ONE.subtract(BigDecimal.valueOf(q)
            .divide(BigDecimal.valueOf(200), 20, RoundingMode.HALF_UP));
        return inverseNormalDistributionFunctionCalculator.getParameterByValue(a);
    }

    /**
     * Рассчитать границы критической области по уровню значимости q.
     *
     * @param subsequenceSize - размер подвыборки
     * @param q - уровень значимости
     * @return границы критической области
     */
    public Pair<BigDecimal, BigDecimal> calculateBorders(Integer subsequenceSize,
                                                         Integer q) {
        BigDecimal mu = BigDecimal.valueOf((long) subsequenceSize * subsequenceSize)
            .divide(BigDecimal.TWO, 20, RoundingMode.HALF_UP);
        BigDecimal du = BigDecimal.valueOf((long) subsequenceSize * subsequenceSize)
            .multiply(BigDecimal.valueOf(2L * subsequenceSize + 1))
            .divide(BigDecimal.valueOf(12), 20, RoundingMode.HALF_UP);
        BigDecimal sigmaU = du.sqrt(MathContext.DECIMAL128)
            .setScale(20, RoundingMode.HALF_UP);

        BigDecimal a = BigDecimal.ONE.subtract(BigDecimal.valueOf(q)
            .divide(BigDecimal.valueOf(200), 20, RoundingMode.HALF_UP));
        BigDecimal t = inverseNormalDistributionFunctionCalculator.getParameterByValue(a);

        BigDecimal leftBorder = mu.subtract(t.multiply(sigmaU));
        BigDecimal rightBorder = mu.add(t.multiply(sigmaU));
        return Pair.of(leftBorder, rightBorder);
    }
}

package polytech.math.statistics.coursework;

import org.apache.commons.lang3.tuple.Pair;

import polytech.math.statistics.coursework.calculation.DispersionCalculator;
import polytech.math.statistics.coursework.calculation.MathExpectationCalculator;
import polytech.math.statistics.coursework.calculation.MedianEstimationCalculator;
import polytech.math.statistics.coursework.calculation.VariationRangeCalculator;
import polytech.math.statistics.coursework.chisquare.ChiSquareAnalyzer;
import polytech.math.statistics.coursework.chisquare.ChiSquareCalculator;
import polytech.math.statistics.coursework.grouping.IntervalCalculator;
import polytech.math.statistics.coursework.grouping.calculation.EmpiricalVarianceCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalAsymmetryCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalAverageCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalExcessCalculator;
import polytech.math.statistics.coursework.grouping.calculation.IntervalModeCalculator;
import polytech.math.statistics.coursework.grouping.calculation.SheppardCorrectionEmpiricalVarianceCalculator;
import polytech.math.statistics.coursework.grouping.calculation.VariationCoefficientCalculator;
import polytech.math.statistics.coursework.grouping.component.Interval;
import polytech.math.statistics.coursework.homogeneity.SignCriteriaCalculator;
import polytech.math.statistics.coursework.io.FileDataReader;
import polytech.math.statistics.coursework.io.NumberedValue;
import polytech.math.statistics.coursework.kolmogorovsmirnov.KolmSmirnovAnalyzer;
import polytech.math.statistics.coursework.kolmogorovsmirnov.KolmSmirnovCalculator;
import polytech.math.statistics.coursework.quantile.QuantileMethodCalculator;
import polytech.math.statistics.coursework.trusting.DispersionTrustedIntervalCalculator;
import polytech.math.statistics.coursework.trusting.MathExpectationTrustIntervalCalculator;
import polytech.math.statistics.coursework.wilcoxon.WilcoxonCriteriaCalculator;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.List;

/**
 * Главный класс для запуска приложения.
 */
public class Application {
    /**
     * Название первого файла с нумерованными денормализованными
     * числами из примера отчета.
     */
    private static final String TEST_DATA = "data/example_report_data.txt";

    public static void main(String[] args) {
        var fileDataReader = new FileDataReader();

        final List<BigDecimal> firstNumbers = fileDataReader.readNumbers(TEST_DATA)
            .stream().map(NumberedValue::getValue).toList();
        final List<BigDecimal> firstVariationSeries = firstNumbers.stream().sorted().toList();

        System.out.println("\n--- Блок 1. Оценка математического ожидания, дисперсии, медианы ---");
        var mathExpectationCalculator = new MathExpectationCalculator();
        System.out.println("Среднее арифметическое (по денормализованным числам): " + mathExpectationCalculator
            .calculateMathExpectation(firstNumbers));
        var dispersionCalculator = new DispersionCalculator();
        System.out.println("Смещенная оценка дисперсии по всей выборке (не в квадрате) (по денормализованным числам): " + dispersionCalculator
            .calculateBiasedEstimation(firstNumbers));
        System.out.println("Несмещенная оценка дисперсии (не в квадрате) (по денормализованным числам): " + dispersionCalculator
            .calculateUnbiasedEstimation(firstNumbers));
        var medianEstimationCalculator = new MedianEstimationCalculator();
        System.out.println("Медиана (по числам вариационного ряда): " + medianEstimationCalculator
            .calculateMedianEstimation(firstVariationSeries));
        var variationRangeCalculator = new VariationRangeCalculator();
        System.out.println("Размах варьирования (по числам вариационного ряда): " + variationRangeCalculator
            .calculateVariationRange(firstVariationSeries));

        System.out.println("\n--- Блок 2. Группирование, графики статистических распределений ---");
        var intervalCalculator = new IntervalCalculator((numbers, intervalWidth) -> numbers.get(0)
            .subtract(BigDecimal.valueOf((double) intervalWidth / 2.0)));
        
        // CHANGE ME: Здесь можно задать желаемое количество интервалов
        int intervalCount = 11;
        // В calculateIntervals тоже можно задать всякие приколы
        // В конечном итоге подберите параметры так, чтобы в последнем интервале накопленная частотность
        // была единица, а в предпоследнем - меньше единицы
        List<Interval> intervals = intervalCalculator.calculateIntervals(firstVariationSeries, intervalCount);
        System.out.println("Интервалы (по числам вариационного ряда):");
        for (var interval : intervals) {
            System.out.println(interval);
        }

        System.out.println("\n--- Блок 3. Точечная оценка характеристик распределения по сгруппированным данным ---");
        var intervalAverageCalculator = new IntervalAverageCalculator();
        System.out.println("Среднее арифметическое (по интервалам для чисел вариационного ряда): " + intervalAverageCalculator
            .calculateIntervalAverage(intervals));
        var intervalModeCalculator = new IntervalModeCalculator();
        System.out.println("Мода (по интервалам для чисел вариационного ряда): " + intervalModeCalculator.calculateIntervalMode(intervals));
        var empiricalVarianceCalculator = new EmpiricalVarianceCalculator();
        System.out.println("Эмпирическая дисперсия и среднее квадратическое отклонение (по интервалам для чисел вариационного ряда): "
            + empiricalVarianceCalculator.calculateEmpiricalVariance(intervals));
        var variationCoefficientCalculator = new VariationCoefficientCalculator();
        System.out.println("Коэффициент вариации (по интервалам для чисел вариационного ряда): " + variationCoefficientCalculator
            .calculateVariationCoefficient(intervals));
        var sheppardCorrectionEmpiricalVarianceCalculator = new SheppardCorrectionEmpiricalVarianceCalculator();
        System.out.println("Оценка дисперсии с учетом смещения Шеппарда (по интервалам для чисел вариационного ряда, в квадрате): "
            + sheppardCorrectionEmpiricalVarianceCalculator.calculateEmpiricalVarianceWithCorrection(intervals));
        System.out.println("Оценка дисперсии с учетом смещения Шеппарда (по интервалам для чисел вариационного ряда): "
            + sheppardCorrectionEmpiricalVarianceCalculator.calculateEmpiricalVarianceWithCorrection(intervals).sqrt(new MathContext(10, RoundingMode.HALF_UP)));
        var intervalAsymmetryCalculator = new IntervalAsymmetryCalculator();
        System.out.println("Асимметрия (по интервалам для чисел вариационного ряда): " + intervalAsymmetryCalculator
            .calculateIntervalAsymmetry(intervals));
        var intervalExcessCalculator = new IntervalExcessCalculator();
        System.out.println("Эксцесс (по интервалам для чисел вариационного ряда): " + intervalExcessCalculator.calculateIntervalExcess(intervals));

        System.out.println("\n--- Блок 4. Оценка параметров распределения методом квантилей --- ");
        var quantileMethodCalculator = new QuantileMethodCalculator();

        int firstIntervalNumber = 2;
        int secondIntervalNumber = 6;

        System.out.println("Параметр m, полученный методом квантилей (по интервалам для чисел вариационного ряда): " + quantileMethodCalculator
            .calculateQuantileMethodParameters(intervals, firstIntervalNumber, secondIntervalNumber).getLeft());
        System.out.println("Параметр сигма, полученный методом квантилей (по интервалам для чисел вариационного ряда): " + quantileMethodCalculator
            .calculateQuantileMethodParameters(intervals, firstIntervalNumber, secondIntervalNumber).getRight());

        final List<BigDecimal> secondNumbers = firstNumbers.stream().limit(20).toList();
        final List<BigDecimal> secondVariationSeries = secondNumbers.stream().sorted().toList();

        System.out.println("\n--- Блок  5. Построение доверительных интервалов ---");
        var mathExpectationTrustIntervalCalculator = new MathExpectationTrustIntervalCalculator();
        System.out.println("-- Доверительный интервал для математического ожидания --");
        System.out.println("Среднее X (по числам вариационного ряда n = 20): " + mathExpectationTrustIntervalCalculator
            .calculateAverage(secondVariationSeries));
        System.out.println("Дисперсия s (по числам вариационного ряда n = 20): " + mathExpectationTrustIntervalCalculator
            .calculateDispersion(secondVariationSeries));

        BigDecimal q5 = BigDecimal.valueOf(2.093);
        BigDecimal q1 = BigDecimal.valueOf(2.861);
        BigDecimal q10 = BigDecimal.valueOf(1.729);

        System.out.println("При q = 5%, t_5,19 = 2.093 (по числам вариационного ряда n = 20): "
            + mathExpectationTrustIntervalCalculator
            .calculateLeftBorder(secondVariationSeries, q5)
            + " < m_x < " + mathExpectationTrustIntervalCalculator
            .calculateRightBorder(secondVariationSeries, q5));
        System.out.println("При q = 1%, t_1,19 = 2.861 (по числам вариационного ряда n = 20): "
            + mathExpectationTrustIntervalCalculator
            .calculateLeftBorder(secondVariationSeries, q1)
            + " < m_x < " + mathExpectationTrustIntervalCalculator
            .calculateRightBorder(secondVariationSeries, q1));
        System.out.println("При q = 10%, t_10,19 = 1.729 (по числам вариационного ряда n = 20): "
            + mathExpectationTrustIntervalCalculator
            .calculateLeftBorder(secondVariationSeries, q10)
            + " < m_x < " + mathExpectationTrustIntervalCalculator
            .calculateRightBorder(secondVariationSeries, q10));
        System.out.println("-- Доверительный интервал для дисперсии и среднего квадратического отклонения --");
        var dispersionTrustedIntervalCalculator = new DispersionTrustedIntervalCalculator();

        BigDecimal q5X1 = BigDecimal.valueOf(8.9065);
        BigDecimal q5X2 = BigDecimal.valueOf(32.8523);

        System.out.println("При q = 5%, X1 = 8.9065, X2 = 32.8523 (по числам вариационного ряда n = 20): \n\t"
            + dispersionTrustedIntervalCalculator
            .calculateSquaredLeftBorder(secondVariationSeries, q5X2)
            + " < (сигма_x)^2 < " + dispersionTrustedIntervalCalculator
            .calculateSquaredRightBorder(secondVariationSeries, q5X1));
        System.out.println("\t" + dispersionTrustedIntervalCalculator
            .calculateLeftBorder(secondVariationSeries, q5X2)
            + " < сигма_x < " + dispersionTrustedIntervalCalculator
            .calculateRightBorder(secondVariationSeries, q5X1));

        BigDecimal q1X1 = BigDecimal.valueOf(6.84397);
        BigDecimal q1X2 = BigDecimal.valueOf(38.58226);

        System.out.println("При q = 1%, X1 = 6.84397, X2 = 38.58226 (по числам вариационного ряда n = 20): \n\t"
            + dispersionTrustedIntervalCalculator
            .calculateSquaredLeftBorder(secondVariationSeries, q1X2)
            + " < (сигма_x)^2 < " + dispersionTrustedIntervalCalculator
            .calculateSquaredRightBorder(secondVariationSeries, q1X1));
        System.out.println("\t" + dispersionTrustedIntervalCalculator
            .calculateLeftBorder(secondVariationSeries, q1X2)
            + " < сигма_x < " + dispersionTrustedIntervalCalculator
            .calculateRightBorder(secondVariationSeries, q1X1));

        BigDecimal q10X1 = BigDecimal.valueOf(10.1170);
        BigDecimal q10X2 = BigDecimal.valueOf(30.1435);

        System.out.println("При q = 10%, X1 = 10.1170, X2 = 30.1435 (по числам вариационного ряда n = 20): \n\t"
            + dispersionTrustedIntervalCalculator
            .calculateSquaredLeftBorder(secondVariationSeries, q10X2)
            + " < (сигма_x)^2 < " + dispersionTrustedIntervalCalculator
            .calculateSquaredRightBorder(secondVariationSeries, q10X1));
        System.out.println("\t" + dispersionTrustedIntervalCalculator
            .calculateLeftBorder(secondVariationSeries, q10X2)
            + " < сигма_x < " + dispersionTrustedIntervalCalculator
            .calculateRightBorder(secondVariationSeries, q10X1));

        System.out.println("\n--- Блок 6. Критерий хи-квадрат для проверки статистических гипотез ---");
        {
            ChiSquareCalculator chiSquareCalculator = new ChiSquareCalculator();
            var localExpectance = mathExpectationTrustIntervalCalculator
            .calculateAverage(secondVariationSeries);
            var localDeviation = mathExpectationTrustIntervalCalculator
            .calculateDispersion(secondVariationSeries);
            var wholeDeviation = dispersionCalculator
            .calculateBiasedEstimation(firstNumbers);
            var table = chiSquareCalculator.calculateChiSquareTable(intervals, localExpectance, localDeviation, wholeDeviation);
            System.out.print(table);
            System.out.println("Количество степеней свободы k = " + table.getK());
            System.out.println("Величина Хи Квадрата X = " + table.getPsiSquare());
            ChiSquareAnalyzer analyzer = new ChiSquareAnalyzer();
            System.out.println("Проверим гипотезу о нормальности распределения");
            System.out.print("При q = 1% ");
            {
                var k = table.getK();
                var q = BigDecimal.valueOf(0.01);
                var isBad = analyzer.isPsiSquareContraveneNormDistribution(
                            table.getPsiSquare(), k, q);
                if (isBad)
                    System.out.println("Наблюдается противоречие: X > Xk, Xk = " + analyzer.getTablePsi(k, q));
                else
                    System.out.println("Не противоречит: X < Xk, Xk = " + analyzer.getTablePsi(k, q));
            }
            System.out.print("При q = 5% ");
            {
                var k = table.getK();
                var q = BigDecimal.valueOf(0.05);
                var isBad = analyzer.isPsiSquareContraveneNormDistribution(
                            table.getPsiSquare(), k, q);
                if (isBad)
                    System.out.println("Наблюдается противоречие: X > Xk, Xk = " + analyzer.getTablePsi(k, q));
                else
                    System.out.println("Не противоречит: X < Xk, Xk = " + analyzer.getTablePsi(k, q));
            }
            System.out.print("При q = 10% ");
            {
                var k = table.getK();
                var q = BigDecimal.valueOf(0.1);
                var isBad = analyzer.isPsiSquareContraveneNormDistribution(
                            table.getPsiSquare(), k, q);
                if (isBad)
                    System.out.println("Наблюдается противоречие: X > Xk, Xk = " + analyzer.getTablePsi(k, q));
                else
                    System.out.println("Не противоречит: X < Xk, Xk = " + analyzer.getTablePsi(k, q));
            }
        }
        {
            System.out.println("\n--- Блок 7. Проверка гипотезы об однородности выборки с помощью критериев знаков и Вилкоксона ---");
            System.out.println("-- Критерий знаков --");
            var signCriteriaCalculator = new SignCriteriaCalculator();
            int sizeOfSubsequences = 20;
            Pair<Integer, Integer> parameters = signCriteriaCalculator
                .calculateSignCriteria(firstNumbers, sizeOfSubsequences);
            System.out.println("k_n(+) (по денормализованным числам): " + parameters.getLeft());
            System.out.println("k_n(-) (по денормализованным числам): " + parameters.getRight());
            System.out.println("-- Критерий Вилкоксона --");
            var wilcoxonCriteriaCalculator = new WilcoxonCriteriaCalculator();
            System.out.println("Число инверсий: " + wilcoxonCriteriaCalculator
                .calculateInversionsNumber(firstNumbers, sizeOfSubsequences));
            System.out.println("При q = 5%:");
            System.out.println("\tt: " + wilcoxonCriteriaCalculator.calculateTValue(5));
            int q = 5;
            var wilcoxonBorders = wilcoxonCriteriaCalculator.calculateBorders(20, q);
            System.out.println("\tu <= " + wilcoxonBorders.getLeft() + "\n\tu >= " + wilcoxonBorders.getRight());

            System.out.println("При q = 1%:");
            q = 1;
            System.out.println("\tt: " + wilcoxonCriteriaCalculator.calculateTValue(1));
            wilcoxonBorders = wilcoxonCriteriaCalculator.calculateBorders(20, q);
            System.out.println("\tu <= " + wilcoxonBorders.getLeft() + "\n\tu >= " + wilcoxonBorders.getRight());

            System.out.println("При q = 10%:");
            q = 10;
            System.out.println("\tt: " + wilcoxonCriteriaCalculator.calculateTValue(10));
            wilcoxonBorders = wilcoxonCriteriaCalculator.calculateBorders(20, q);
            System.out.println("\tu <= " + wilcoxonBorders.getLeft() + "\n\tu >= " + wilcoxonBorders.getRight());
        }
        
        System.out.println("\n--- Блок 8. Проверка гипотезы о нормальном распределении с помощью критерия Колмогорова-Смирнова ---");
        {
            KolmSmirnovCalculator kolmSmirnovCalculator = new KolmSmirnovCalculator();
            var allExpectance = mathExpectationCalculator
            .calculateMathExpectation(firstNumbers);
            var allDeviation = dispersionCalculator
            .calculateBiasedEstimation(firstNumbers);
            var table = kolmSmirnovCalculator.calculateKolmSmirnovTable(intervals, allExpectance, allDeviation);
            System.out.print(table);
            System.out.println("Максимальное расхождение Dn = " + table.getMaxDifference());
            KolmSmirnovAnalyzer analyzer = new KolmSmirnovAnalyzer();
            System.out.println("Проверим гипотезу о нормальности распределения");
            System.out.print("При q = 1% ");
            {
                var q = BigDecimal.valueOf(0.01);
                var isBad = analyzer.isKolmValueContraveneNormDistribution(
                            table.getMaxDifference(), q);
                if (isBad)
                    System.out.println("Наблюдается противоречие: Dn > Dmax, Dmax = " + analyzer.getTableKolm(q));
                else
                    System.out.println("Не противоречит: Dn < Dmax, Dmax = " + analyzer.getTableKolm(q));
            }
            System.out.print("При q = 5% ");
            {
                var q = BigDecimal.valueOf(0.05);
                var isBad = analyzer.isKolmValueContraveneNormDistribution(
                            table.getMaxDifference(), q);
                if (isBad)
                    System.out.println("Наблюдается противоречие: Dn > Dmax, Dmax = " + analyzer.getTableKolm(q));
                else
                    System.out.println("Не противоречит: Dn < Dmax, Dmax = " + analyzer.getTableKolm(q));
            }
            System.out.print("При q = 10% ");
            {
                var q = BigDecimal.valueOf(0.1);
                var isBad = analyzer.isKolmValueContraveneNormDistribution(
                            table.getMaxDifference(), q);
                if (isBad)
                    System.out.println("Наблюдается противоречие: Dn > Dmax, Dmax = " + analyzer.getTableKolm(q));
                else
                    System.out.println("Не противоречит: Dn < Dmax, Dmax = " + analyzer.getTableKolm(q));
            }
        }
    }
}

package polytech.math.statistics.coursework.chisquare;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.math3.distribution.NormalDistribution;

import polytech.math.statistics.coursework.chisquare.component.ChiSquareTable;
import polytech.math.statistics.coursework.chisquare.component.ChiSquareTableRow;
import polytech.math.statistics.coursework.grouping.component.Interval;

/**
 * Класс для расчёта хи-квадрата
 */
public class ChiSquareCalculator {
    /**
     * Рассчитать хи-квадрат для заданных интервалов с использованием
     * нормального распределения.
     *
     * @param intervals      - интервалы
     * @param expectance     - математическое ожидание, полученное из первых n
     *                       элементов таблицы. Из раздела доверительный интервал
     * @param deviation      - дисперсия, рассчитанная из первых n элементов
     *                       таблицы.
     *                       Из раздела доверительный интервал
     * @param wholeDeviation - дисперсия, рассчитанная из всех элементов таблицы
     * @return таблицу связанную с хи-квадратом
     */
    public ChiSquareTable calculateChiSquareTable(List<Interval> intervals, BigDecimal expectance,
            BigDecimal deviation, BigDecimal wholeDeviation) {
        ChiSquareTable resultTable = new ChiSquareTable();
        var nExperiments = calculateNumberOfExperiments(intervals);
        var normalDistribution = new NormalDistribution();
        for (var interval : intervals) {
            var rowBuilder = ChiSquareTableRow.builder();
            rowBuilder.withInterval(interval);

            var deviationFraction = (interval.getEnd().subtract(expectance).divide(wholeDeviation, 8,
                    RoundingMode.HALF_UP));
            rowBuilder.withDeviationFraction(deviationFraction);

            var right = interval.getEnd();
            var left = interval.getStart();
            var lessThanRightProb = normalDistribution
                    .cumulativeProbability(
                            right.subtract(expectance).divide(deviation, 8, RoundingMode.HALF_UP).doubleValue());
            var lessThanLeftProb = normalDistribution
                    .cumulativeProbability(
                            left.subtract(expectance).divide(deviation, 8, RoundingMode.HALF_UP).doubleValue());
            var probability = BigDecimal.valueOf(lessThanRightProb - lessThanLeftProb);
            rowBuilder.withProbability(probability);

            var localExpectance = probability.multiply(BigDecimal.valueOf(nExperiments));
            rowBuilder.withMathExpectance(localExpectance);

            var frequencyExpectanceDifference = BigDecimal.valueOf(interval.getFrequency()).subtract(localExpectance);
            rowBuilder.withFrequencyExpectanceDifference(frequencyExpectanceDifference);

            var psiSquare = frequencyExpectanceDifference.multiply(frequencyExpectanceDifference)
                    .divide(localExpectance, 8, RoundingMode.HALF_UP);
            rowBuilder.withPsiSquare(psiSquare);
            resultTable.addRow(rowBuilder.build());
        }
        var newRows = collapseRows(resultTable.getRows());
        resultTable.setRows(newRows);
        resultTable.setK(newRows.size() - 2 - 1);
        return resultTable;
    }

    private Integer calculateNumberOfExperiments(List<Interval> intervals) {
        int nExperiments = 0;
        for (var interval : intervals) {
            nExperiments += interval.getFrequency();
        }
        return Integer.valueOf(nExperiments);
    }

    /**
     * Крч данный метод схлопывает интервалы с краёв мат ожиданиях которых меньше
     * пяти
     */
    private ArrayList<ChiSquareTableRow> collapseRows(List<ChiSquareTableRow> rows) {
        ArrayList<ChiSquareTableRow> newRows = new ArrayList<>();
        // В методичке сказано пять, но я оставил чтобы можно было менять
        // Схлопывает интервалы если ожидание меньше чем...
        double minAllowedExpectance = 5;
        newRows.add(rows.get(0));
        { // Схлопываем первую половину интервалов
            int i = 1;
            for (; i < rows.size() / 2; ++i) {
                if (newRows.getLast().getMathExpectance().compareTo(BigDecimal.valueOf(minAllowedExpectance)) < 0 ||
                        rows.get(i).getMathExpectance().compareTo(BigDecimal.valueOf(minAllowedExpectance)) < 0) {
                    var lastRow = newRows.removeLast();
                    newRows.addLast(lastRow.unite(rows.get(i)));
                } else {
                    newRows.add(rows.get(i));
                }
            }

            for (; i < rows.size(); ++i) {
                newRows.add(rows.get(i));
            }
        }
        { // Схлопываем вторую половину интервалов
            rows = List.copyOf(newRows);
            newRows.clear();
            newRows.addLast(rows.getLast());
            int i = rows.size() - 2;
            for (; i > rows.size()/2; --i) {
                if (newRows.getFirst().getMathExpectance().compareTo(BigDecimal.valueOf(minAllowedExpectance)) < 0 ||
                        rows.get(i).getMathExpectance().compareTo(BigDecimal.valueOf(minAllowedExpectance)) < 0) {
                    var firstRow = newRows.removeFirst();
                    newRows.addFirst(rows.get(i).unite(firstRow));
                } else {
                    newRows.addFirst(rows.get(i));
                 }
            }

            for (; i >= 0; --i) {
                newRows.addFirst(rows.get(i));
            }

            return newRows;
        }
    }
}

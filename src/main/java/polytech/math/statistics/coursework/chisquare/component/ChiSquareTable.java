package polytech.math.statistics.coursework.chisquare.component;

import java.math.BigDecimal;
import java.util.ArrayList;

import lombok.Data;

@Data
public class ChiSquareTable {
    private ArrayList<ChiSquareTableRow> rows = new ArrayList<>();

    private Integer k = Integer.valueOf(0);
    
    /**
     * Добавляет строчку в конец таблицы
     */
    public void addRow(ChiSquareTableRow row) {
        rows.addLast(row);
    }

    /**
     * @return Величину Хи квадрата.
     */
    public BigDecimal getPsiSquare() {
        BigDecimal psiSquare = BigDecimal.ZERO;
        for (var row : rows) {
            psiSquare = psiSquare.add(row.getPsiSquare());
        }
        return psiSquare;
    }
    
    @Override
    public String toString() {
        StringBuilder resultStr = new StringBuilder();
        String[] headers = new String[] { "N", "Начало", "Конец", "В долях s", "Вероятность", "Мат ожидание", "Частота", "Уклонение", "Хи Квадрат" };
        resultStr.append(String.format("%2s%7s%15s%15s%15s%15s%15s%15s%15s%n", (Object[])headers));
        for (int i = 0; i < rows.size(); ++i) {
            resultStr.append(String.format("%2s", String.valueOf(i+1)));
            resultStr.append(rows.get(i).toString());
            resultStr.append("\n");
        }
        return resultStr.toString();
    }
}

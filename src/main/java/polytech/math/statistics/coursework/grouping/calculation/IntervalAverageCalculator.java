package polytech.math.statistics.coursework.grouping.calculation;

import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета среднего арифметического
 * по заданным интервалам.
 */
public class IntervalAverageCalculator {
    /**
     * Рассчитать среднее арифметическое по заданным интервалам.
     *
     * @param intervals - интервалы
     * @return среднее арифметическое
     */
    public BigDecimal calculateIntervalAverage(List<Interval> intervals) {
        int elementsNumber = intervals.stream()
            .mapToInt(Interval::getFrequency).sum();

        BigDecimal average = BigDecimal.ZERO;
        for (Interval interval : intervals) {
            average = average.add(interval.getMiddle()
                .multiply(BigDecimal.valueOf(interval.getFrequency())));
        }

        return average.divide(BigDecimal.valueOf(elementsNumber),
            10, RoundingMode.HALF_UP);
    }
}

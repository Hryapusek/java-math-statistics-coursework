package polytech.math.statistics.coursework.grouping.component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Objects;

/**
 * Класс интервала со всеми характеристиками.
 */
@Data
@AllArgsConstructor
@Builder(setterPrefix = "with")
public class Interval {
    /**
     * Номер интервала.
     */
    private final Integer number;

    /**
     * Начало интервала.
     */
    private final BigDecimal start;

    /**
     * Конец интервала.
     */
    private final BigDecimal end;

    /**
     * Середина интервала.
     */
    private final BigDecimal middle;

    /**
     * Частота.
     */
    private final Integer frequency;

    /**
     * Частотность.
     */
    private final BigDecimal incidence;

    /**
     * Накопленная частотность.
     */
    private final BigDecimal accumulatedIncidence;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Interval interval = (Interval) o;
        return Objects.equals(number, interval.number)
            && start.setScale(3, RoundingMode.HALF_UP)
            .compareTo(interval.start.setScale(3, RoundingMode.HALF_UP)) == 0
            && end.setScale(3, RoundingMode.HALF_UP)
            .compareTo(interval.end.setScale(3, RoundingMode.HALF_UP)) == 0
            && middle.setScale(3, RoundingMode.HALF_UP)
            .compareTo(interval.middle.setScale(3, RoundingMode.HALF_UP)) == 0
            && Objects.equals(frequency, interval.frequency)
            && incidence.setScale(3, RoundingMode.HALF_UP)
            .compareTo(interval.incidence.setScale(3, RoundingMode.HALF_UP)) == 0
            && accumulatedIncidence.setScale(3, RoundingMode.HALF_UP)
            .compareTo(interval.accumulatedIncidence.setScale(3, RoundingMode.HALF_UP)) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, start, end, middle, frequency, incidence, accumulatedIncidence);
    }

    @Override
    public String toString() {
        return "Интервал #" + number + " {\n\tНачало: "
            + start + ",\n\t" + "Конец: "
            + end + ",\n\t" + "Середина: "
            + middle + ",\n\t" + "Частота: "
            + frequency + ",\n\t" + "Частотность: "
            + incidence + ",\n\t" + "Накопленная частотность: "
            + accumulatedIncidence + ",\n}\n";
    }
}

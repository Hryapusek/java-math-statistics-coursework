package polytech.math.statistics.coursework.grouping;

import java.math.BigDecimal;
import java.util.List;

@FunctionalInterface
public interface FirstIntervalStartCalculator {
    /**
     * Рассчитать начало для первого интервала.
     *
     * @param numbers       - числа вариационного ряда
     * @param intervalWidth - ширина интервала
     * @return начало первого интервала
     */
    BigDecimal calculateFirstIntervalStart(List<BigDecimal> numbers,
                                           Double intervalWidth);
}

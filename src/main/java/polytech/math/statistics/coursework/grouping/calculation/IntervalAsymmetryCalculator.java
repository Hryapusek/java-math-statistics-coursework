package polytech.math.statistics.coursework.grouping.calculation;

import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета асимметрии.
 */
public class IntervalAsymmetryCalculator {
    /**
     * Класс для расчета среднего арифметического
     * по заданным интервалам.
     */
    private final IntervalAverageCalculator intervalAverageCalculator =
        new IntervalAverageCalculator();

    /**
     * Класс для расчета эмпирической дисперсии.
     */
    private final EmpiricalVarianceCalculator empiricalVarianceCalculator =
        new EmpiricalVarianceCalculator();

    /**
     * Рассчитать асимметрию.
     *
     * @param intervals - интервалы
     * @return асимметрия
     */
    public BigDecimal calculateIntervalAsymmetry(List<Interval> intervals) {
        BigDecimal thirdCentralMoment = calculateThirdCentralMoment(intervals);
        return thirdCentralMoment.divide(empiricalVarianceCalculator
            .calculateEmpiricalVariance(intervals).pow(3), 10, RoundingMode.HALF_UP);
    }

    /**
     * Рассчитать третий центральный момент.
     * @param intervals - интервалы
     * @return асимметрия
     */
    private BigDecimal calculateThirdCentralMoment(List<Interval> intervals) {
        BigDecimal average = intervalAverageCalculator.calculateIntervalAverage(intervals);
        BigDecimal numerator = BigDecimal.ZERO;

        for (Interval interval : intervals) {
            BigDecimal difference = interval.getMiddle().subtract(average);
            numerator = numerator.add(difference.pow(3)
                .multiply(BigDecimal.valueOf(interval.getFrequency())));
        }

        return numerator.divide(BigDecimal.valueOf(intervals.stream().mapToInt(Interval::getFrequency).sum()),
            10, RoundingMode.HALF_UP);
    }
}

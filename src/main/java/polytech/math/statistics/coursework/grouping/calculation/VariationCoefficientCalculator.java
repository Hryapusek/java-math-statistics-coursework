package polytech.math.statistics.coursework.grouping.calculation;

import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Класс для расчета коэффициента вариации.
 */
public class VariationCoefficientCalculator {
    /**
     * Класс для расчета среднего арифметического
     * по заданным интервалам.
     */
    private final IntervalAverageCalculator intervalAverageCalculator =
        new IntervalAverageCalculator();

    /**
     * Класс для расчета эмпирической дисперсии.
     */
    private final EmpiricalVarianceCalculator empiricalVarianceCalculator =
        new EmpiricalVarianceCalculator();

    /**
     * Рассчитать коэффициент вариации для интервалов.
     *
     * @param intervals - интервалы
     * @return коэффициент вариации
     */
    public BigDecimal calculateVariationCoefficient(List<Interval> intervals) {
        BigDecimal average = intervalAverageCalculator
            .calculateIntervalAverage(intervals);
        BigDecimal empiricalVariance = empiricalVarianceCalculator
            .calculateEmpiricalVariance(intervals);

        return empiricalVariance.divide(average, 10, RoundingMode.HALF_UP);
    }
}

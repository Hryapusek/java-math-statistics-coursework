package polytech.math.statistics.coursework.grouping;

import lombok.RequiredArgsConstructor;
import polytech.math.statistics.coursework.calculation.VariationRangeCalculator;
import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Класс для группировки данных и расчета
 * всех параметров для всех интервалов.
 */
@RequiredArgsConstructor
public class IntervalCalculator {
    /**
     * Класс для расчета размаха варьирования.
     */
    private final VariationRangeCalculator variationRangeCalculator =
        new VariationRangeCalculator();

    private final FirstIntervalStartCalculator firstIntervalStartCalculator;

    /**
     * Рассчитать интервалы и их характеристики.
     *
     * @param numbers - числа вариационного ряда
     * @return интервалы
     */
    public List<Interval> calculateIntervals(List<BigDecimal> numbers,
                                             Integer intervalCount) {
        // CHANGE ME: Здесь вы можете задать свою ширину интервалов
        double intervalWidth = variationRangeCalculator
            .calculateVariationRange(numbers).divide(BigDecimal.valueOf(intervalCount),
                0, RoundingMode.UP).doubleValue();

        List<Interval> intervals = new ArrayList<>();
        // CHANGE ME: Здесь вы можете задать свою точку старта интервалов
        // EXAMPLE: BigDecimal start = BigDecimal.valueOf(800);
        BigDecimal start = firstIntervalStartCalculator.calculateFirstIntervalStart(numbers, intervalWidth);
        BigDecimal accumulatedIncidence = BigDecimal.ZERO;

        for (int currentIntervalNumber = 0; currentIntervalNumber < intervalCount; currentIntervalNumber++) {
            BigDecimal end = start.add(BigDecimal.valueOf(intervalWidth));
            BigDecimal middle = start.add(end).divide(BigDecimal.TWO, 10, RoundingMode.HALF_UP);
            Integer frequency = calculateIntervalFrequency(numbers, start, end);
            BigDecimal incidence = BigDecimal.valueOf(frequency)
                .divide(BigDecimal.valueOf(numbers.size()), 10, RoundingMode.HALF_UP);
            accumulatedIncidence = accumulatedIncidence.add(incidence);

            intervals.add(Interval.builder()
                .withNumber(currentIntervalNumber + 1)
                .withStart(start)
                .withEnd(end)
                .withMiddle(middle)
                .withFrequency(frequency)
                .withIncidence(incidence)
                .withAccumulatedIncidence(accumulatedIncidence)
                .build());

            start = start.add(BigDecimal.valueOf(intervalWidth));
        }

        return intervals;
    }

    /**
     * Рассчитать частоту интервала.
     *
     * @param numbers       - числа вариационного ряда
     * @param intervalStart - начало интервала
     * @param intervalEnd   - конец интервала
     * @return частота интервала
     */
    private Integer calculateIntervalFrequency(List<BigDecimal> numbers,
                                               BigDecimal intervalStart,
                                               BigDecimal intervalEnd) {
        AtomicInteger frequency = new AtomicInteger(0);
        numbers.forEach(number -> {
            if (number.compareTo(intervalStart) > 0
                && number.compareTo(intervalEnd) <= 0) {
                frequency.incrementAndGet();
            }
        });

        return frequency.get();
    }
}

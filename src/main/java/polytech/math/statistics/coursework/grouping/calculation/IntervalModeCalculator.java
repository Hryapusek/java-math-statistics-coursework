package polytech.math.statistics.coursework.grouping.calculation;

import polytech.math.statistics.coursework.grouping.component.Interval;

import java.math.BigDecimal;
import java.util.List;

/**
 * Класс для вычисления моды интервалов.
 */
public class IntervalModeCalculator {
    @SuppressWarnings("OptionalGetWithoutIsPresent")
    public BigDecimal calculateIntervalMode(List<Interval> intervals) {
        return intervals.stream().reduce((left, right) -> {
            if (left.getFrequency() > right.getFrequency()) {
                return left;
            }

            return right;
        }).get().getMiddle();
    }
}
